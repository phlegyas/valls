﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowScript : MonoBehaviour {
	public Rigidbody myRigidbody;
	public float throwSpeed;
	public float speed;
	public float lastMouseX;
	public float lastMouseY;
	public Vector3 newPosition;
	public bool isAlreadyTouched;
	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody> ();
		speed = 10f;
		throwSpeed = 50f;
		isAlreadyTouched = false;
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began){
		if (Input.GetMouseButtonDown (0)) {
			isAlreadyTouched = true;
			Debug.Log("Pressed left click.");
		}
//		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended){
		if (Input.GetMouseButtonUp (0)) {
//			Throw (Input.GetTouch(0).position);
			Throw (Input.mousePosition);
//			Debug.Log("END");
		}
		if (Input.GetKeyDown(KeyCode.R)) {
			Debug.Log("Reset");
			Reset ();
		}
//		if (Input.touchCount == 1) {
		if (Input.GetMouseButton(0)) {
//			Debug.Log("isAlreadyTouched == true");
			lastMouseX = Input.mousePosition.x;
//			lastMouseX = Input.GetTouch(0).position.x;
			lastMouseY = Input.mousePosition.y;
//			lastMouseY = Input.GetTouch(0).position.y;
		}
	}

	void Reset(){
		transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.5f, 0.1f, Camera.main.nearClipPlane * 7.5f));
		newPosition = transform.position;
		myRigidbody.useGravity = false;
		myRigidbody.velocity = Vector3.zero;
		myRigidbody.angularVelocity = Vector3.zero;
		transform.rotation = Quaternion.Euler (0f, 0f, 0f);
//		transform.SetParent (Camera.main.transform);
	}

	void OnMouseDrag(){
		//		Debug.Log("MousDown");
//		if (Input.touchCount == 1) {
				
//		Vector3 mousePos = Input.GetTouch (0).position;
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = Camera.main.nearClipPlane * 10f;

		newPosition = Camera.main.ScreenToWorldPoint (mousePos);
		transform.localPosition = Vector3.Lerp (transform.localPosition, newPosition, 50f * Time.deltaTime);
//		}
	}

	void Throw(Vector3 mousePos){
		myRigidbody.useGravity = true;

		float diffOnY = (mousePos.y - lastMouseY) / Screen.height * 10;

		speed = throwSpeed * diffOnY;
	
		float x = (mousePos.x / Screen.width) - (lastMouseX / Screen.width);
		Debug.Log ("x1        = ");
		Debug.Log (x);
		Debug.Log ("mp - lmx  = ");
		Debug.Log (Input.mousePosition.x - lastMouseX);
//		x = Mathf.Abs(Input.GetTouch(0).position.x - lastMouseX) / Screen.width * 10 * x;
		x = Mathf.Abs(Input.mousePosition.x - lastMouseX) / Screen.width * 100 * x;

		Vector3 direction = new Vector3(x, 0f, 5f);
		direction = Camera.main.transform.TransformDirection (direction);

		myRigidbody.AddForce ((direction * speed / 2f) + (Vector3.up * speed));
		Debug.Log ("x2        = ");
		Debug.Log (x);
		Debug.Log ("direction = ");
		Debug.Log (direction);
		Debug.Log ("diffOnY   = ");
		Debug.Log (diffOnY);


//		Debug.Log("Throw");
		//Vector3 direction = new Vector3 (plop, 0f, 1f);
		//direction = Camera.main.transform.TransformDirection(direction);
		//myRigidbody.AddForce ((direction * speed / 2f) + (Vector3.up * speed));
		//myRigidbody.AddForce ((direction * speed / 2f) + (Vector3.up * speed));
	}
}
