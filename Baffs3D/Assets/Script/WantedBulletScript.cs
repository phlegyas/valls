﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WantedBulletScript : MonoBehaviour {

	public Camera mainCamera;
	public GameObject myHole;
	public GameObject InvisiblePlane;
	public Text myText;
	public bool IsHit;
	// Use this for initialization
	void Start () {
		IsHit = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log("CLICK");
			click ();
		}
	}

	void click() {
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 100)) {
			//This instantiate a bullet hol on the "Wanted" picutre WESTERN LIKE
			GameObject obj = Instantiate (myHole) as GameObject;
			obj.transform.parent = this.transform;
			obj.transform.position = hit.point;
			obj.transform.rotation = Quaternion.identity;
			obj.transform.localScale = new Vector3(0.5F, 0.5F, 0.5F);
			if (IsHit == false) {
				Great ();
				StartCoroutine(LoadNextLevel ());
			}
		}
		IsHit = true;
	}

	void Great(){
		Debug.Log("THIS");
		Debug.Log(myText.text);
		myText.text = "GREAT";
		myText.color = Color.green;
	}

	IEnumerator LoadNextLevel () {
		yield return new WaitForSeconds (3f);
		SceneManager.LoadScene ("scene01");
	}
}
