﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public GameObject manu;
	float timeLeft;
	public float cd;
	Vector3 mousePosition;
	public GameObject Honda;
	public float moveSpeed = 0.1f;
	public RectTransform bar;
	public int nextLevel;
	public SpriteRenderer back;
	float max;
	// Use this for initialization
	void Start () {
		if (cd == 0)
			cd = 1;
		timeLeft = cd;
		nextLevel = 0;
		max = bar.rect.width;
		bar.sizeDelta = new Vector2 (0, bar.rect.height);
//		bar.rect.Set (bar.rect.x, bar.rect.y, 0, bar.rect.height);
		back.color = new Color(1f,1f,1f,.5f);
	}

	public void getPoint(){
		bar.sizeDelta = new Vector2 (bar.rect.width + (max / 100) * 5, bar.rect.height);
		if (bar.rect.width == max)
			Application.Quit ();
	}

	// Update is called once per frame
	void Update () {

		timeLeft -= Time.deltaTime;
		if ( timeLeft < 0 )
		{
			timeLeft = Time.deltaTime + cd;
			GameObject newmanu = Instantiate (manu);
			newmanu.GetComponent<Rigidbody2D> ().gravityScale = 1;
			newmanu.GetComponent<BoxCollider2D> ().enabled = true;
			newmanu.transform.position = new Vector3 (Random.Range (-9.10f, 9.10f), manu.transform.position.y, 0);
		}
		Vector3 move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
		if (Input.GetAxis ("Horizontal") < 0 && Honda.transform.localScale.x > 0)
			Honda.transform.localScale = new Vector3 (Honda.transform.localScale.x * -1, Honda.transform.localScale.y, Honda.transform.localScale.z);
		else if (Input.GetAxis ("Horizontal") > 0 && Honda.transform.localScale.x < 0)
			Honda.transform.localScale = new Vector3 (Honda.transform.localScale.x * -1, Honda.transform.localScale.y, Honda.transform.localScale.z);
		
		Honda.transform.position += move * moveSpeed * Time.deltaTime;
		if (Honda.transform.position.x > 9.46)
			Honda.transform.position = new Vector3 (9.46f, Honda.transform.position.y, 0);
		if (Honda.transform.position.x < -9.46)
			Honda.transform.position = new Vector3 (-9.46f, Honda.transform.position.y, 0);
		if (Honda.transform.position.y > 4.46)
			Honda.transform.position = new Vector3 (Honda.transform.position.x, 4.46f, 0);
		if (Honda.transform.position.y < -4.46)
			Honda.transform.position = new Vector3 (Honda.transform.position.x, -4.46f, 0);
		
	}
}
