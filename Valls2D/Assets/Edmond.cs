﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edmond : MonoBehaviour {
	AudioSource[] source;
	AudioSource[] tauntsound;
	public GameManager shared;
	public GameObject taunt;

	// Use this for initialization
	void Start () {
		source = GetComponents<AudioSource> ();
		tauntsound = taunt.GetComponents<AudioSource> ();
	}
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.transform.position.x - transform.position.x > 0)
			coll.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (1000, 0));
		else
			coll.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-1000, 0));
		shared.getPoint ();
		source [Random.Range (0, 2)].Play ();
		if (Random.Range (0, 2) >= 1)
			tauntsound [Random.Range (0, 2)].Play ();
		coll.gameObject.GetComponents<AudioSource> () [Random.Range (0, 4)].Play ();
	}
	// Update is called once per frame
	void Update () {
		
	}
}
